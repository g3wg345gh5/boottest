package com.example.demo.controller;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping(produces = "application/json")
public class AppRestController {

    @GetMapping("/app-name")
    @ResponseBody
    public Map<String, String> appName(ServerHttpRequest request) {
        return Collections.singletonMap("appName", "testboot1123123456789.123.1");
    }

}
